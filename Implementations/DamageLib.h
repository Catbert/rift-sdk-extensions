#pragma once

#pragma warning(push, 0)
#pragma warning(disable: 4365)
#include <map>
#pragma warning(pop)

#include "SDK Extensions.h"

#pragma region Passives
struct PassiveDamageResult {
	PassiveDamageResult() { this->Valid = false; }
	PassiveDamageResult(float physicalDamage, float magicalDamage, float trueDamage, 
		float physicalDamagePercent, float magicalDamagePercent, float trueDamagePercent)
	{
		this->Valid = true;
		this->PhysicalDamage = std::floor(physicalDamage);
		this->MagicalDamage  = std::floor(magicalDamage);
		this->TrueDamage	 = std::floor(trueDamage);

		this->PhysicalDamagePercent = physicalDamagePercent;
		this->MagicalDamagePercent  = magicalDamagePercent;
		this->TrueDamagePercent		= trueDamagePercent;
	}

	bool Valid;
	bool NextAttackPhysical = false;
	bool NextAttackMagical = false;
	bool NextAttackTrue = false;
	float PhysicalDamage;
	float MagicalDamage;
	float TrueDamage;
	float PhysicalDamagePercent;
	float MagicalDamagePercent;
	float TrueDamagePercent;
};

enum PassiveDamageType {
	FlatPhysical,
	FlatMagical,
	FlatTrue,
	PercentPhysical,
	PercentMagical,
	PercentTrue,
	None
};

typedef float(*DamageLambda_t) (AIHeroClient*, AIBaseClient*);
struct DamagePassive {
	PassiveDamageType DamageType;
	DamageLambda_t PassiveDamage;
	const char * Name;	
	PassiveDamageType NextAttack;

	DamagePassive(PassiveDamageType damageType, DamageLambda_t passiveDamage, const char * name = NULL, PassiveDamageType nextAttack = PassiveDamageType::None) {
		DamageType = damageType;
		PassiveDamage = passiveDamage;
		Name = name;
		NextAttack = nextAttack;
	}
};
#pragma endregion

class DamageLib : public IDamageLib {
	static std::vector<DamagePassive> StaticPassives;
	static std::vector<DamagePassive> DynamicPassives;
	static std::map<std::string, std::map<std::pair<unsigned char, SkillStage>, DamageLambda_t>> SpellDamageDB;

	static PassiveDamageResult GetStaticItemDamage(AIHeroClient* Source, bool MinionTarget);
	static PassiveDamageResult GetDynamicItemDamage(AIHeroClient* Source, AIBaseClient* Target);
	static PassiveDamageResult ComputeItemDamage(AIHeroClient* Source, AIBaseClient* Target);

	static PassiveDamageResult GetStaticPassiveDamage(AIHeroClient* Source, bool MinionTarget);
	static PassiveDamageResult GetDynamicPassiveDamage(AIHeroClient* Source, AIBaseClient* Target);
	static PassiveDamageResult ComputePassiveDamage(AIHeroClient* Source, AIBaseClient* Target);

	static float GetPassivePercentMod(AIBaseClient* Source, AIBaseClient* Target, int Type);

	
	static void RegisterSpellDamage(std::string& charName, unsigned char Slot, SkillStage Stage, DamageLambda_t Lambda);
public:
	DamageLib();

	void InitSpells();
	void InitPassives();	

	float GetSpellDamage(AIHeroClient* Source, AIBaseClient* Target, unsigned char Slot, SkillStage Stage);
	StaticAttackDamage GetStaticAutoAttackDamage(AIHeroClient* Source, bool MinionTarget);
	float GetAutoAttackDamage(AIBaseClient* Source, AIBaseClient* Target, StaticAttackDamage* StaticDmg = NULL, bool CheckPassives = false);
	float GetAutoAttackDamage(AIBaseClient* Source, AIBaseClient* Target, bool CheckPassives = false);	

	float CalculateMagicalDamage(AIBaseClient * Source, AIBaseClient * Target, float Amount);
	float CalculatePhysicalDamage(AIBaseClient * Source, AIBaseClient * Target, float Amount);

	static float GetAutoAttackDamageInternal(AIBaseClient* Source, AIBaseClient* Target, StaticAttackDamage* StaticDmg, bool CheckPassives);
};






