#include "EventHandler.h"

unsigned long EventHandler::LastTick;
std::map<CallbackEnum, bool> EventHandler::IsRunning;

std::queue<std::pair<std::function<void()>, long int>> EventHandler::DelayedActionsQueue;
std::vector<std::pair<std::function<void()>, long int>> EventHandler::DelayedActions;

std::vector<RENDERSCENECALLBACK>		EventHandler::TickCallbacks;
	 
std::vector<PREMOVECALLBACK	  >			EventHandler::PreMoveCallbacks;
std::vector<PREATTACKCALLBACK >			EventHandler::PreAttackCallbacks;
std::vector<PRECASTCALLBACK   >			EventHandler::PreCastCallbacks;
std::vector<POSTATTACKCALLBACK>			EventHandler::PostAttackCallbacks;
std::vector<UNKILLABLECALLBACK>			EventHandler::UnkillableCallbacks;

std::map<unsigned int, bool> EventHandler::IsVisible;
std::vector<VISIONCALLBACK>				EventHandler::GainVisionCallbacks;
std::vector<VISIONCALLBACK>				EventHandler::LoseVisionCallbacks;

std::vector<INTERRUPTIBLECALLBACK>	EventHandler::InterruptibleCallbacks;
std::vector<DASHCALLBACK>			EventHandler::DashCallbacks;
std::vector<GAPCLOSERCALLBACK>		EventHandler::GapcloserCallbacks;

#define REGISTER_HANDLER(FUNC) \
if (IsRunning.count(t) == 0) { \
FUNC; \
IsRunning[t] = true; \
} 

void EventHandler::RegisterCallback(CallbackEnum t, void* func, void* UserData) {
	switch (t) {
	case (CallbackEnum::CreateObject):
		SdkRegisterCallback((GAMEOBJECTSCALLBACK)func, UserData, CALLBACK_TYPE_OBJECT_CREATE, CALLBACK_POSITION_BACK);		
		break;
	case (CallbackEnum::DeleteObject):
		SdkRegisterCallback((GAMEOBJECTSCALLBACK)func, UserData, CALLBACK_TYPE_OBJECT_DELETE, CALLBACK_POSITION_FRONT);
		break;
	case (CallbackEnum::NewPath):
		SdkRegisterCallback((ONAIMOVECALLBACK)func, UserData, CALLBACK_TYPE_AI_MOVE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::Attack):
		SdkRegisterCallback((ONAIATTACKCALLBACK)func, UserData, CALLBACK_TYPE_AI_ATTACK, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::Update):
		SdkRegisterCallback((RENDERSCENECALLBACK)func, UserData, CALLBACK_TYPE_GAME_SCENE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::Overlay):
		SdkRegisterCallback((RENDERSCENECALLBACK)func, UserData, CALLBACK_TYPE_OVERLAY_SCENE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::SpellCastEnd):
		SdkRegisterCallback((ONAICASTATTACKCALLBACK)func, UserData, CALLBACK_TYPE_AI_CAST_ATTACK, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::SpellCastStart):
		SdkRegisterCallback((ONAICASTATTACKCALLBACK)func, UserData, CALLBACK_TYPE_AI_PROCESS_SPELL, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::Recall):
		SdkRegisterCallback((ONUNITRECALLCALLBACK)func, UserData, CALLBACK_TYPE_UNIT_RECALL, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::InventoryUpdate):
		SdkRegisterCallback((ONPLAYERSHOPCALLBACK)func, UserData, CALLBACK_TYPE_PLAYER_SHOP, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::BuffUpdate):
		SdkRegisterCallback((ONAIBUFFUPDATECALLBACK)func, UserData, CALLBACK_TYPE_AI_BUFF_UPDATE, CALLBACK_POSITION_BACK);
		break;
	case (CallbackEnum::BuffCreateAndDelete):
		SdkRegisterCallback((ONAIBUFFCREATEDELETECALLBACK)func, UserData, CALLBACK_TYPE_AI_BUFF_CREATE_DELETE, CALLBACK_POSITION_BACK);
		break;


	case (CallbackEnum::Tick):
		LastTick = 0UL;
		REGISTER_HANDLER(SdkRegisterGameScene(TickHandler, UserData));
		TickCallbacks.push_back((RENDERSCENECALLBACK)func);
		break;
	case (CallbackEnum::PreAttack):
		PreAttackCallbacks.push_back((PREATTACKCALLBACK)func);
		break;
	case (CallbackEnum::PreMove):
		PreMoveCallbacks.push_back((PREMOVECALLBACK)func);
		break;
	case (CallbackEnum::PreCast):
		PreCastCallbacks.push_back((PRECASTCALLBACK)func);
		break;
	case (CallbackEnum::PostAttack):
		PostAttackCallbacks.push_back((POSTATTACKCALLBACK)func);
		break;
	case (CallbackEnum::GainVision):
		GainVisionCallbacks.push_back((VISIONCALLBACK)func);
		REGISTER_HANDLER(RegisterCallback(CallbackEnum::Tick, VisionHandler));
		break;
	case (CallbackEnum::LoseVision):
		LoseVisionCallbacks.push_back((VISIONCALLBACK)func);
		t = CallbackEnum::GainVision;
		REGISTER_HANDLER(RegisterCallback(CallbackEnum::Tick, VisionHandler));
		break;
	case (CallbackEnum::UnkillableMinion):
		UnkillableCallbacks.push_back((UNKILLABLECALLBACK)func);
		break;
	case (CallbackEnum::Interruptible):
		InterruptibleCallbacks.push_back((INTERRUPTIBLECALLBACK)func);
		REGISTER_HANDLER(RegisterCallback(CallbackEnum::Tick, InterruptibleHandler));
		break;
	case (CallbackEnum::Dash):
		DashCallbacks.push_back((DASHCALLBACK)func);
		REGISTER_HANDLER(RegisterCallback(CallbackEnum::Tick, DashHandler));
		break;
	case (CallbackEnum::GapCloser):
		GapcloserCallbacks.push_back((GAPCLOSERCALLBACK)func);
		t = CallbackEnum::Dash;
		REGISTER_HANDLER(RegisterCallback(CallbackEnum::Tick, DashHandler));
		break;
	}
}

void EventHandler::DelayedAction(std::function<void()> func, int delay) {
	DelayedActionsQueue.push(std::make_pair(func, Common::GetTickCount() + (unsigned int)delay));
	constexpr auto t{ CallbackEnum::Tick };
	REGISTER_HANDLER(SdkRegisterGameScene(TickHandler, NULL));
}

#define FIRE_CALLBACKS(CONTAINER, ...) \
if (!CONTAINER.empty()) {\
    for (auto it = CONTAINER.begin(); it != CONTAINER.end();) { \
        auto func = *it; \
        auto result {SdkGetLoadedModule(func, NULL, NULL, NULL)}; \
        if (SDKSTATUS_SUCCESS(result)) { \
            func(__VA_ARGS__); \
            ++it; \
        } \
        else { it = CONTAINER.erase(it); }\
    }\
}


void EventHandler::TickHandler(void* UserData) {
	unsigned long Tick = Common::GetTickCount();
	if ((Tick - EventHandler::LastTick) > (1000 / 30)) {
		EventHandler::LastTick = Tick;
		FIRE_CALLBACKS(TickCallbacks, UserData);		
	}	

	while (!DelayedActionsQueue.empty()) {
		DelayedActions.push_back(DelayedActionsQueue.front());
		DelayedActionsQueue.pop();
	}
	
	if (!DelayedActions.empty()) {
		for (auto it = DelayedActions.begin(); it < DelayedActions.end();) {
			auto &func = it->first;
			auto time = (unsigned long)it->second;
			if (Tick > time) {					
				auto result{ SdkGetLoadedModule(Common::GetLambdaAddress(func), NULL, NULL, NULL) };				
				if (SDKSTATUS_SUCCESS(result)) {func(); }
					it = DelayedActions.erase(it);
			}
			else { ++it; }
		}
	}
};

void EventHandler::DashHandler(void* AI, bool Move, bool Stop, void* UserData) {
	UNREFERENCED_PARAMETER(Move);
	UNREFERENCED_PARAMETER(Stop);
	UNREFERENCED_PARAMETER(UserData);

	GameObject* obj{ pSDK->EntityManager->GetObjectFromPTR(AI) };
	AIHeroClient* Hero{ (obj != NULL) ? obj->AsAIHeroClient() : NULL };
	if (Hero && Hero->IsDashing()) {
		auto nav{ Hero->NavInfo() };
		FIRE_CALLBACKS(DashCallbacks, Hero, &(nav.StartPos), &(nav.EndPos) , Common::GetTickCount(), (unsigned int) (nav.EndPos.Distance(nav.StartPos) / nav.DashSpeed * 1000.0f), nav.DashSpeed);			
	}
}

void EventHandler::VisionHandler(void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

	auto Enemies{ pSDK->EntityManager->GetEnemyHeroes() };
	for (auto &[_, Enemy] : Enemies) {
		auto nID = Enemy->GetNetworkID();		
		if (EventHandler::IsVisible.count(nID) == 0) {
			EventHandler::IsVisible[nID] = Enemy->IsVisible();
		}
		else if (EventHandler::IsVisible[nID] && !Enemy->IsVisible()) {
			EventHandler::IsVisible[nID] = false;
			FIRE_CALLBACKS(LoseVisionCallbacks, Enemy);			
		}
		else if (!EventHandler::IsVisible[nID] && Enemy->IsVisible()) {
			EventHandler::IsVisible[nID] = true;
			FIRE_CALLBACKS(GainVisionCallbacks, Enemy);									
		}		
	}	
};

void EventHandler::InterruptibleHandler(void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

};

void EventHandler::FirePreMove(bool* Process, Vector3* Position) {
	FIRE_CALLBACKS(PreMoveCallbacks, Process, Position);	
}
void EventHandler::FirePreAttack(bool* Process, AttackableUnit** Target) {
	FIRE_CALLBACKS(PreAttackCallbacks, Process, Target);	
}
void EventHandler::FirePreCast(bool* Process, PSDK_SPELL Spell, Vector3* Position, AttackableUnit** Target) {
	FIRE_CALLBACKS(PreCastCallbacks, Process, Spell, Position, Target);	
}
void EventHandler::FirePostAttack(AttackableUnit* Target) {
	FIRE_CALLBACKS(PostAttackCallbacks, Target);	
}

void EventHandler::FireUnkillable(AIMinionClient * Target){
	FIRE_CALLBACKS(UnkillableCallbacks, Target);	
}

