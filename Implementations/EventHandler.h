#pragma once

#pragma warning(push, 0)
#pragma warning(disable: 4365)
#include <map>
#include <vector>
#include <queue>
#include <functional>
#pragma warning(pop)

#include "SDK Extensions.h"

class EventHandler : public IEventHandler {
public:
	EventHandler() {};
	~EventHandler() {};

	void RegisterCallback(CallbackEnum t, void* func, void* UserData=NULL);
	void DelayedAction(std::function<void()> func, int delay);
	
	void FirePreMove(bool* Process, Vector3* Position);
	void FirePreAttack(bool* Process, AttackableUnit** Target);
	void FirePreCast(bool* Process, PSDK_SPELL Spell, Vector3* Position, AttackableUnit** Target);
	void FirePostAttack(AttackableUnit* Target);
	void FireUnkillable(AIMinionClient* Target);

	static unsigned long LastTick;
	static std::map<CallbackEnum, bool> IsRunning;

	#pragma region Containers
		static std::queue< std::pair<std::function<void()>, long int > > DelayedActionsQueue;
		static std::vector< std::pair<std::function<void()>, long int > > DelayedActions;
		static std::vector<RENDERSCENECALLBACK> TickCallbacks;		

		static std::vector<INTERRUPTIBLECALLBACK>	InterruptibleCallbacks;
		static std::vector<DASHCALLBACK>			DashCallbacks;
		static std::vector<GAPCLOSERCALLBACK>		GapcloserCallbacks;

		static std::vector<PREMOVECALLBACK  >    PreMoveCallbacks;
		static std::vector<PREATTACKCALLBACK>    PreAttackCallbacks;
		static std::vector<PRECASTCALLBACK  >    PreCastCallbacks;
		static std::vector<POSTATTACKCALLBACK>   PostAttackCallbacks;
		static std::vector<UNKILLABLECALLBACK>   UnkillableCallbacks;

		static std::map<unsigned int, bool> IsVisible;
		static std::vector<VISIONCALLBACK  > GainVisionCallbacks;
		static std::vector<VISIONCALLBACK  > LoseVisionCallbacks;
	#pragma endregion

	#pragma region Handlers
		static void TickHandler(void* UserData);
		static void DashHandler(void* AI, bool Move, bool Stop, void* UserData);	
		static void InterruptibleHandler(void* UserData);
		static void	VisionHandler(void* UserData);
		
	#pragma endregion
};





