#pragma once

#include "SDK Extensions.h"

#pragma region PathTracker
class EnemyData {
public:
	bool IsStopped;
	bool IsWindupChecked;
	
	unsigned int LastWaypointTick;
	unsigned int StopTick;
	unsigned int Count;
	unsigned int LastAATick;
	unsigned int LastWindupTick;
	unsigned int OrbwalkCount;	

	float AvgTick;
	float AvgPathLenght;
	float AvgOrbwalkTime;
	float LastAngleDiff;

	float PathLength;
	std::vector<Vector2> LastWaypoints;

	EnemyData() {
		IsStopped = false;
		LastWaypoints = {};
		LastWaypointTick = GetTickCount();
		StopTick = 0;
		AvgTick = 0;
		AvgPathLenght = 0;
		Count = 0;
		LastAATick = GetTickCount();
		LastWindupTick = GetTickCount();
		IsWindupChecked = false;
		OrbwalkCount = 0;
		AvgOrbwalkTime = 0;
		LastAngleDiff = 0;
	};
	EnemyData(std::vector<Vector2>& wp) : EnemyData() {
		LastWaypoints = wp;		
	}

	unsigned int MovImmobileTime();
	unsigned int LastMovChangeTime();
	float AvgReactionTime();
	float AvgMovChangeTime();
};
class PathTracker {
	inline static const double MaxTime = 1.5;
	static std::map<unsigned int, EnemyData> EnemyInfo;
	static void AnalyzePath(AIHeroClient * Unit);
public:
	static void Init();
	static EnemyData& GetData(AIHeroClient* Unit);

	static unsigned int MovImmobileTime(AIBaseClient* Unit);
	static unsigned int LastMovChangeTime(AIBaseClient* Unit);

	static float AvgReactionTime(AIBaseClient* Unit);
	static float AvgMovChangeTime(AIBaseClient* Unit);
	static float AvgPathLenght(AIBaseClient* Unit);
	static float LastAngleDiff(AIBaseClient* Unit);

	static std::vector<Vector2> CutPath(std::vector<Vector2>& path, float distance);
	static float GetPathLength(std::vector<Vector2>& path);

	static void __cdecl OnCastEnd(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
	static void	__cdecl	OnAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData);
	static void __cdecl OnNewPath(void* AI, bool Move, bool Stop, void* UserData);	
};
#pragma endregion